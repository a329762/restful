const express = require('express');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const total = n1 + n2;
    res.send(`La suma en GET = ${req.params.total}`);
}

function index(req, res, next) {
    res.send(`Get id = ${req.params.id}`);
}

function create(req, res, next) {
    const name= req.body.name;
    const lastName= req.body.lastName;
    res.send(`Crear usuario nuevo con nombre ${name} y apellido ${lastName}`);
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const total = n1 * n2;
    res.send(`Multiplicacion en POST = ${req.params.total}`);
}

function replace(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const total = n1 / n2;
    res.send(`Dividir en PUT = ${req.params.total}`);
}

function edit(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const total = n1 ^ n2;
    res.send(`Potencia en PATCH = ${req.params.total}`);
}

function destroy(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const total = n1 - n2;
    res.send(`Restar con DELETE = ${req.params.total}`);
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}